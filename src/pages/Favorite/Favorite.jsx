
import { useSelector } from "react-redux";
import Card from "../../components/Card/card";
import Modal from '../../components/Modal/Modal'




function Favorite() {

    const favorites = useSelector((state) => state.favorites)


    const styles = {
        fontSize: "28px",
        fontWeight: "600",
        marginBottom: "20px"
    }

    return (
        <>
            <h2 style={styles}>In your favorite:</h2>
                <div >
                {favorites.map(p => <Card
                    key = {p.id}
                    info={p}
                    />)}
                </div>
            <Modal text = 'The product has been successfully added to the cart!' />
        </>
      );
}

export default Favorite;

import { useDispatch, useSelector } from 'react-redux';
import { useContext, useEffect} from 'react';
import { updateProductList} from '../../stores/actions';
import Card from '../../components/Card/card';
import Modal from '../../components/Modal/Modal';
import styles from './Homepage.module.scss'
import Button from '../../components/Button/Button';
import Table from '../../components/Table/Table';
import ProductContext from '../../context';
import TableItem from '../../components/Table/TableItem';


function Homepage() {

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(updateProductList());
  }, [dispatch]);

  const products = useSelector((state) => state.products)
  const {pageView, toggleView } = useContext(ProductContext)

  const productView = () => {
    if(pageView === 'Table'){
      return (
        <div className="products">
          {products.map(p => <Card key = {p.id} info={p} />)}
        </div>)

    }else if(pageView === 'Card'){
      return (
        <div className="products">
          <Table>
          {products.map(item => <TableItem key={item.id} info ={item}/>)}
          </Table>
      </div>
      )
    }
  }

    return (
      <>
          <div>
              <h1 className={styles.title}>The best offers from our store</h1>
              <Button backgroundColor = 'gray' text = {pageView} onClick={toggleView}/>
            </div>
            {productView()}
            <Modal text = 'The product has been successfully added to the cart!' />
      </>

     );
}


export default Homepage;

import styles from './Table.module.scss'

function Table({children}){

 return (
      <table className={styles.table}>
        <thead className={styles.title}>
          <tr>
            <th className={styles.title}>Article</th>
            <th className={styles.title}>Name</th>
            <th className={styles.title}>Color</th>
            <th className={styles.title}>Img</th>
            <th className={styles.title}>Price</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        {children}
        </tbody>
      </table>
    );
  };

  export default Table;


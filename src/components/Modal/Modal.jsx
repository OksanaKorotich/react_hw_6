import { useDispatch, useSelector } from 'react-redux'
import styles from './Modal.module.scss'
import PropTypes from 'prop-types'
import { closeModal } from '../../stores/actions';


function Modal({text}){

    const dispatch = useDispatch();
    const modal = useSelector((state) => state.modal )
    const closeModalWindow = () =>{
        dispatch(closeModal(false))
    }

    return (
        <div data-testid="modal-container" className={modal? styles.active : styles.hidden} onClick={closeModalWindow}>
             <div className={styles.modal__content} onClick={ event => event.stopPropagation()}>
                <span className={styles.modal__text}>{text}</span>
                <button className={styles.modal__action_btn} onClick={closeModalWindow} >ок</button>
            </div>
        </div>
)}


Modal.propTypes = {
    text: PropTypes.string.isRequired,
}

export default Modal